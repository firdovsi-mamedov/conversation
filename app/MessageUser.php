<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MessageUser extends Model
{
    public function users(){
      return $this->belongsTo('App\User', 'sender_id');
    }

    protected $fillable = ['sender_id', 'to_id', 'message', 'conversation_id'];
}
