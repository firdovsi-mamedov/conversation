@extends('layouts.app')


@section('content')

  <div class="container">
    <h1>Message chat area</h1>
    <chat-component user_from="{{Auth::id()}}"></chat-component>
  </div>

@stop
